package com.pearadmin.process.param;

import lombok.Data;

@Data
public class CreateModelParam {

   private String name;

   private String key;

   private String description;

   private String version;

}
