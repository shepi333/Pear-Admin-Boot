package com.pearadmin.system.param;

import lombok.Data;

@Data
public class QueryUserParam {

    private String username;

    private String realName;

}
