package com.pearadmin.system.param;

import lombok.Data;

@Data
public class QueryRoleParam {

    private String roleCode;

    private String roleName;
}
