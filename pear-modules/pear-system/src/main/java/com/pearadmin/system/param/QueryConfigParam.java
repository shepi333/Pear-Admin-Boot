package com.pearadmin.system.param;

import lombok.Data;

@Data
public class QueryConfigParam {

    private String configName;

    private String configCode;
}
